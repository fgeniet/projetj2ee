package ensg.eu.TPSpring.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import ensg.eu.TPSpring.metier.Evenement;
import ensg.eu.TPSpring.metier.Participant;

@Entity
@Table( name = "evenement" )
public class Evenement {
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="id_evenement", strategy = "increment")
	public int idEvenement;
	
	@Column(name = "nom", nullable = false)
	public String nom;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_debut", nullable = false)
	public Date dateDebut;
	
	@ManyToMany(mappedBy = "evenements")
	public List<Participant> listParticipants = new ArrayList<Participant>();

	public Evenement(String nom, Date dateDebut) {
		super();
		this.nom = nom;
		this.dateDebut = dateDebut;
	}
	
	public Evenement() {
		
	}
	
	public boolean hasNoParticipants() {
		return listParticipants.size() == 0;
	}
	

	
	@Override
	public boolean equals(Object o) {
		if ( this == o ) {
			return true;
		}
		if ( o == null || getClass() != o.getClass() ) {
			return false;
		}
		Evenement evenement = (Evenement) o;
		return Objects.equals( nom, evenement.nom ) &&
				Objects.equals( dateDebut, evenement.dateDebut );
	}

}
