package ensg.eu.TPSpring.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table( name = "participant" )
@Inheritance(strategy = InheritanceType.JOINED)
public class Participant {
	
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="id_participant", strategy = "increment")
	public int idParticipant;
	
	@Column(name = "nom", nullable = false)
	public String nom;
	
	@Column(name = "prenom", nullable = false)
	public String prenom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_naissance", nullable = false)
	public Date dateNaissance;
	
	@Column(name = "email", nullable = false)
	public String email;
	
	
	@ManyToMany
	@JoinTable(
	  name = "evenement_participant", 
	  joinColumns = @JoinColumn(name = "id_participant"), 
	  inverseJoinColumns = @JoinColumn(name = "id_evenement"))
	public List<Evenement> evenements = new ArrayList<Evenement>();

	public Participant(String nom, String prenom, String email, Date dateNaissance) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.dateNaissance = dateNaissance;
	}
	
	public Participant() {
		
	}
	
	@Override
	public String toString() {
		String result = prenom + " " + nom + " (" + email + ")";
		return result;
	}
}
