package ensg.eu.TPSpring.metier;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "intervenant")
@PrimaryKeyJoinColumn(name = "id_participant")
public class Intervenant extends Participant{
	
	@Column(name = "id_intervenant", nullable = false)
	public int idIntervenant;
	
	@Column(name = "nombre_heures", nullable = false)
	public int nombreHeures;
	
	@Column(name = "besoins_specifiques", nullable = false)
	public String besoinsSpecifiques;	
	
	public Intervenant(String nom, String prenom, String email, Date dateNaissance, int idIntervenant, int nombresHeures, String besoinsSpecifiques) {
		super(nom, prenom, email, dateNaissance);
		this.idIntervenant = idIntervenant;
		this.nombreHeures = nombresHeures;
		this.besoinsSpecifiques = besoinsSpecifiques;
	}
	
	public Intervenant() {
		
	}

}
