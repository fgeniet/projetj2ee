package ensg.eu.TPSpring.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import ensg.eu.TPSpring.metier.Evenement;
import ensg.eu.TPSpring.metier.Intervenant;
import ensg.eu.TPSpring.services.EvenementRepository;
import ensg.eu.TPSpring.services.IntervenantRepository;

@Controller
public class IntervenantController {
	
	@Autowired
	private IntervenantRepository intervenantRepository;
	@Autowired
	private EvenementRepository evenementRepository;
	
	@GetMapping("/liste_intervenants")
	public String listeIntervenants(Model model) {
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		model.addAttribute("listeIntervenants", intervenantRepository.findAll());
			
		return "liste_intervenants";
	}
	
	@GetMapping("/gestion_intervenants")
	public String gestionIntervenants(Model model) {
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		model.addAttribute("listeIntervenants", intervenantRepository.findAll());
		
		return "gestion_intervenants";
	}
	
	@GetMapping("/ajout_intervenant")
	public String ajoutIntervenant(
			@RequestParam(name="nom", required=true) String nom, 
			@RequestParam(name="prenom", required=true) String prenom, 
			@RequestParam(name="email", required=true) String email, 
			@RequestParam(name="date_naissance", required=true) String date_naissance, 
			@RequestParam(name="evenement", defaultValue = "")List<String> listeId,
			@RequestParam(name="id_intervenant", required=true) String idIntervenant,
			@RequestParam(name="nombreHeures", required=true) String nombreHeures,
			@RequestParam(name="besoinsSpecifiques", required=true) String besoinsSpecifiques,
			Model model) {
		
		String[] date = date_naissance.split("-");
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1]);
		int day = Integer.parseInt(date[2]);
		Intervenant newIntervenant = new Intervenant(nom, prenom, email, new Date(year, month, day), Integer.parseInt(idIntervenant), Integer.parseInt(nombreHeures), besoinsSpecifiques);
		for (String id : listeId) {
			Optional<Evenement> evenement = evenementRepository.findById(Integer.parseInt(id));
			if (!evenement.isEmpty()) {
				newIntervenant.evenements.add(evenement.get());
			}
		}
		intervenantRepository.save(newIntervenant);
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		model.addAttribute("listeIntervenants", intervenantRepository.findAll());
		
		return "gestion_intervenants";
	}
	
	@GetMapping(value = "/supprimer_intervenant/{id}")
	public String supprimerIntervenant(@PathVariable int id, Model model) {
		
		intervenantRepository.deleteById(id);
			
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		model.addAttribute("listeIntervenants", intervenantRepository.findAll());
		
		return "gestion_intervenants";
	}
	
	@GetMapping(value = "/modifier_intervenant/{id}")
	public String modifierIntervenant(@PathVariable int id, Model model) {
		
		Optional<Intervenant> intervenant = intervenantRepository.findById(id);
		
		if (intervenant.isEmpty()) {
			model.addAttribute("listeEvenements", evenementRepository.findAll());
			model.addAttribute("listeIntervenants", intervenantRepository.findAll());
			
			return "gestion_intervenants";
		}
		
		else {
			model.addAttribute("listeEvenements", evenementRepository.findAll());
			model.addAttribute("intervenant", intervenant.get());
			
			return "modifier_intervenant";
		}

	}
	
	@GetMapping(value = "/sauvegarder_modification_intervenant/{idIntervenant}")
	public String sauvegarderModification(
			@PathVariable int idIntervenant,
			@RequestParam(name="nom", required=true) String nom, 
			@RequestParam(name="prenom", required=true) String prenom, 
			@RequestParam(name="email", required=true) String email, 
			@RequestParam(name="date_naissance", required=true) String date_naissance, 
			@RequestParam(name="evenement", defaultValue = "")List<String> listeId,
			@RequestParam(name="id_intervenant", required=true) String idInt,
			@RequestParam(name="nombreHeures", required=true) String nombreHeures,
			@RequestParam(name="besoinsSpecifiques", required=true) String besoinsSpecifiques,
			Model model) {
		
		Optional<Intervenant> intervenant = intervenantRepository.findById(idIntervenant);
		
		if (!intervenant.isEmpty()) {
			
			Intervenant intervenantModifie = intervenant.get();
			
			String[] date = date_naissance.split("-");
			int year = Integer.parseInt(date[0]);
			int month = Integer.parseInt(date[1]);
			int day = Integer.parseInt(date[2]);
			
			intervenantModifie.nom = nom;
			intervenantModifie.prenom = prenom;
			intervenantModifie.email = email;
			intervenantModifie.dateNaissance = new Date(year, month, day);
			
			intervenantModifie.evenements.clear();			
			for (String id : listeId) {
				Optional<Evenement> evenement = evenementRepository.findById(Integer.parseInt(id));
				if (!evenement.isEmpty()) {
					intervenantModifie.evenements.add(evenement.get());
				}
			}
			
			intervenantModifie.idIntervenant = Integer.parseInt(idInt);
			intervenantModifie.nombreHeures = Integer.parseInt(nombreHeures);
			intervenantModifie.besoinsSpecifiques = besoinsSpecifiques;
			intervenantRepository.save(intervenantModifie);
		}
		
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		model.addAttribute("listeIntervenants", intervenantRepository.findAll());
		
		return "gestion_intervenants";
	}
	
}
