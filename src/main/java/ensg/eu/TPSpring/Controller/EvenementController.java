package ensg.eu.TPSpring.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import ensg.eu.TPSpring.metier.Evenement;
import ensg.eu.TPSpring.metier.Participant;
import ensg.eu.TPSpring.services.EvenementRepository;

@Controller
public class EvenementController {
	
	@Autowired
	private EvenementRepository evenementRepository;
	
	@GetMapping("/liste_evenements")
	public String listeEvenements(Model model) {
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
			
		return "liste_evenements";
	}
	
	@GetMapping("/gestion_evenements")
	public String gestionEvenements(Model model) {
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		return "gestion_evenements";
	}
	
	@GetMapping("/ajout_evenement")
	public String ajoutEvenement(
			@RequestParam(name="nom", required=true) String nom, 
			@RequestParam(name="date_debut", required=true) String dateDebut, 
			Model model) {
		
		String[] date = dateDebut.split("-");
		int year = Integer.parseInt(date[0]);
		int month = Integer.parseInt(date[1]);
		int day = Integer.parseInt(date[2]);
		Evenement newEvenement = new Evenement(nom, new Date(year, month, day));
		evenementRepository.save(newEvenement);
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		return "gestion_evenements";
	}
	
	@GetMapping(value = "/supprimer_evenement/{id}")
	public String supprimerEvenement(@PathVariable int id, Model model) {
		
		evenementRepository.deleteById(id);
			
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		return "gestion_evenements";
	}
	
	@GetMapping(value = "/modifier_evenement/{id}")
	public String modifierEvenement(@PathVariable int id, Model model) {
		
		Optional<Evenement> evenement = evenementRepository.findById(id);
		
		if (evenement.isEmpty()) {
			model.addAttribute("listeEvenements", evenementRepository.findAll());
			
			return "gestion_evenements";
		}
		
		else {
			model.addAttribute("listeEvenements", evenementRepository.findAll());
			model.addAttribute("evenement", evenement.get());
			
			return "modifier_evenement";
		}

	}
	
	@GetMapping(value = "/sauvegarder_modification_evenement/{idEvenement}")
	public String sauvegarderModification(
			@PathVariable int idEvenement,
			@RequestParam(name="nom", required=true) String nom, 
			@RequestParam(name="date_debut", required=true) String date_debut, 
			Model model) {
		
		Optional<Evenement> evenement = evenementRepository.findById(idEvenement);
		
		if (!evenement.isEmpty()) {
			
			Evenement evenementModifie = evenement.get();
			
			String[] date = date_debut.split("-");
			int year = Integer.parseInt(date[0]);
			int month = Integer.parseInt(date[1]);
			int day = Integer.parseInt(date[2]);
			
			evenementModifie.nom = nom;
			evenementModifie.dateDebut = new Date(year, month, day);
			
			evenementRepository.save(evenementModifie);
		}
		
		
		model.addAttribute("listeEvenements", evenementRepository.findAll());
		
		return "gestion_evenements";
	}
	
}
